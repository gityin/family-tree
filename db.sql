-- familytree_db.tree definition

CREATE TABLE `tree` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '姓名',
  `L` int(6) DEFAULT NULL,
  `R` int(6) DEFAULT NULL,
  `dorder` int(6) DEFAULT NULL COMMENT '排序',
  `sex` varchar(255) DEFAULT NULL COMMENT '性别',
  `wname` varchar(255) DEFAULT NULL COMMENT '配偶',
  `dad` varchar(255) DEFAULT NULL COMMENT '父亲',
  `mother` varchar(255) DEFAULT NULL COMMENT '母亲',
  `dc` smallint(6) unsigned DEFAULT NULL COMMENT '第多少代',
  `zibei` varchar(255) DEFAULT NULL COMMENT '字辈',
  `rank` varchar(255) DEFAULT NULL COMMENT '家中长幼次序',
  `line` varchar(6) NOT NULL DEFAULT '' COMMENT '家中排行',
  `brother` varchar(255) DEFAULT NULL COMMENT '兄弟',
  `sisters` varchar(255) DEFAULT NULL COMMENT '姐妹',
  `son` varchar(255) DEFAULT NULL COMMENT '儿子',
  `daughter` varchar(255) DEFAULT NULL COMMENT '女儿',
  `year` varchar(255) DEFAULT NULL COMMENT '生日',
  `dieday` varchar(255) DEFAULT NULL COMMENT '祭日',
  `lifeadresse` varchar(255) DEFAULT NULL COMMENT '居住地',
  `mudi` varchar(255) DEFAULT NULL COMMENT '墓地',
  `tongxun` varchar(255) DEFAULT NULL COMMENT '通讯',
  `nation` varchar(255) DEFAULT NULL COMMENT '国籍',
  `birthadresse` varchar(255) DEFAULT NULL COMMENT '出生地',
  `jiguan` varchar(255) DEFAULT NULL COMMENT '籍贯',
  `rname` varchar(255) DEFAULT NULL COMMENT '曾用名',
  `marriage` varchar(255) DEFAULT NULL COMMENT '婚姻',
  `zhiye` varchar(255) DEFAULT NULL COMMENT '职业',
  `xueli` varchar(255) DEFAULT NULL COMMENT '学历',
  `other` text,
  `jisi` varchar(255) DEFAULT NULL COMMENT '继嗣',
  `info` text COMMENT '平生简介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;




-- familytree_db.zibei definition

CREATE TABLE `zibei` (
  `id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='辈字';




-- familytree_db.users definition

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO users
(id, username, password, created_at)
VALUES(1, 'admin', '$2y$10$ZGOOsB9FbXrd6iiB7.obHO6dxx3W6De0GTt2Cdz.zAzB88GLmS9Q.', NULL);
-- 配一个后台登陆账号admin    123