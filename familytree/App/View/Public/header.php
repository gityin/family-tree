<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $title;?></title>
    <meta name="description" content="SinglePHP" />

    <link rel="stylesheet" type="text/css" href="/Statics/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="/Statics/css/bootstrap.min.css"/>
    <!--主要样式-->
    <script src="/Statics/js/context-menu.js" type="text/javascript" ></script>
    <script src="/Statics/js/jquery-1.7.2.min.js" type="text/javascript" ></script>
    <script src="/Statics/images/layer/layer.js" type="text/javascript"></script>
    <?php

    if(isset($css)) foreach($css as $path){
        echo "<link rel='stylesheet' href='$path'>";
    }
    if(isset($js)) foreach($js as $path){
        echo "<script src='$path'></script>";
    }
    ?>
    <script type="text/javascript">
        $(function(){
            $('.tree li:has(ul)').addClass('parent_li').find(' > span').attr('title', '收起子孙族谱树');
            $('.tree li.parent_li > span').on('click', function (e) {
                var children = $(this).parent('li.parent_li').find(' > ul > li');
                if (children.is(":visible")) {
                    children.hide('fast');
                    $(this).attr('title', '展开子孙族谱树').find(' > i').addClass('icon-plus-sign').removeClass('icon-minus-sign');
                } else {
                    children.show('fast');
                    $(this).attr('title', '收起子孙族谱树').find(' > i').addClass('icon-minus-sign').removeClass('icon-plus-sign');
                }
                e.stopPropagation();
            });
        });
    </script>
</head>
<body>