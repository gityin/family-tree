<?php
$data = array(
    'title' => empty($title)?C('SITE_TITLE'):$title,
);
?>
<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #F2F2F2;
    }

    h1 {
        text-align: center;
        color: #333;
    }

    form {
        max-width: 400px;
        margin: 0 auto;
        background-color: #FFF;
        padding: 20px;
        border-radius: 10px;
        box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
    }

    label {
        display: block;
        margin-bottom: 10px;
        color: #666;
    }

    input[type="text"],
    input[type="password"] {
        width: 100%;
        padding: 10px;
        border-radius: 5px;
        border: none;
        margin-bottom: 20px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.1);
    }

    input[type="submit"] {
        background-color: #4CAF50;
        color: #FFF;
        border: none;
        border-radius: 5px;
        padding: 10px;
        cursor: pointer;
    }

    input[type="submit"]:hover {
        background-color: #3e8e41;
    }

    .error{
        text-align: center;
        margin-bottom: 10px;
        color: red;
    }
</style>
<main class="bs-docs-masthead" id="content" role="main">
    <h1>管理员登录</h1>
    <?php if (isset($error)) { ?>
        <div class="error"><?php echo $error; ?></div>
    <?php } ?>
    <form action="/admin/login/login" method="post">
        <label>用户名:</label>
        <input type="text" name="username"><br>
        <label>密码:</label>
        <input type="password" name="password"><br>
        <input type="submit" value="登录">
    </form>
</main>

