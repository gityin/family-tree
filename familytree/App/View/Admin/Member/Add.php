<?php
$data = array(
    'title' => empty($title)?C('SITE_TITLE'):$title,
    'css'=>$css
);
View::tplInclude('Public/header', $data); ?>
<style type="text/css">
    body{line-height:25px;font-size:14px;margin:0;}
    #wp{margin:0 auto;width:90%;padding:0 10px;border:1px #ccc solid;}
    h1{margin:0;padding:10px;font-size:20px;}
    h2{border-top:1px #ccc solid;border-bottom:1px #ccc solid;background:#eee;margin:10px -10px;padding:5px 8px;font-size:14px;}
    ul{list-style:none;margin:0;padding:0;}
    ul.view_node,ul.manage_node{padding:10px 0;}
    ul.view_node li{padding-left:1em;}
    ul.manage_node{text-align:right;}
    ul.manage_node li{clear:both;}
    ul.manage_node p{float:left;margin:0;}
    ul.manage_node ul{padding-left:1em;}
    .gray{color:gray;}
    a{text-decoration:none;}
    li.hover{background:#efefef;}
    input,button{cursor:pointer;}
</style>
<script type="text/javascript">
    $.post=function(data,callback){
        return $.ajax({
            url:'/Admin/Member/AjaxSave',
            type:'POST',
            data:data,
            dataType:'json',
            success:function(json){
                if(json.code==0){
                    callback(json);
                }else{
                    alert(json.msg);
                }
            },
            error:function(xhr){
                $('#errorMsg').html(xhr.responseText+'<hr/>');
                setTimeout(function(){$('#errorMsg').html('')},10000);
            }
        });
    }
    function tree_add(ipt,pid){
        var val=$.trim($(ipt).val());
        if(!/.+/.test(val)){
            alert('不能空！');
            $(ipt).focus();
            return;
        }
        $.post({pid:pid,name:val,op:4},function(status){
            location.reload();
        });
    };
</script>
<main class="bs-docs-masthead" id="content" role="main">
    <div id="wp">
        <ul class="view_node">
            <h2>节点管理
                <li>
                    <a href="/index.php">首页</a>&nbsp;<a href="/Admin/Home">编辑详情</a>
                </li>
            </h2>
            <div id="errorMsg"></div>
            <ul class="manage_node">
                <?php W('Tree', ['currTree'=>$tree[0],'list'=>$tree,'tpl'=>'EditTreeWidget']);?>
            </ul>
        </ul>
    </div>
</main>
<script type="text/javascript">
    $('li').mouseover(function(e){
        $(this).addClass('hover');
        e.stopPropagation();
    }).mouseout(function(e){
        $(this).removeClass('hover');
        e.stopPropagation();
    });

    $('.tree_add').click(function(){
        var ul=$(this).parent().children('ul');
        if(ul.size()==0){
            ul=$('<ul/>').appendTo($(this).parent());
        }
        $('#ipt_add_child').parent().remove();
        $('<li><p><input id="ipt_add_child" type="text" value="" size="30"/><input type="button" value="添加" onclick="tree_add(\'#ipt_add_child\','+$(this).parent().attr('nodeid')+')"/></p>&nbsp;</li>').appendTo(ul);
    });
    $('.tree_drop').click(function(){
        $.post({id:$(this).parent().attr('nodeid'),op:1},function(status){
            location.reload();
        });
    });

    //上移/下移节点
    function tree_swap_recursion(selt){
        selt.each(function(){
            var $this=$(this);
            var hasPrev=$this.prev().size();
            var hasNext=$this.next().size();

            var span=$('<span> -</span>').insertAfter($this.children('.tree_moveto'));
            var id=$this.attr('nodeid');
            if(hasPrev){
                span.append(' <a href="javascript:void(\'上移节点\');" title="上移节点" class="tree_move_up" onclick="tree_swap('+$this.prev().attr('nodeid')+','+id+')">上移</a>');
            }else{
                span.append(' <span class="gray" title="上移节点">上移</span>');
            }
            if(hasNext){
                span.append(' <a href="javascript:void(\'下移节点\');" title="下移节点" class="tree_move_down" onclick="tree_swap('+id+','+$this.next().attr('nodeid')+')">下移</a>');
            }else{
                span.append(' <span class="gray" title="下移节点">下移</span>');
            }

            tree_swap_recursion($(this).children('ul').children('li'));
        });
    }
    function tree_swap(id,id2){
        $.post({id:id,id2:id2,op:2},function(status){
            location.reload();
        });
    }
    tree_swap_recursion($('ul.manage_node > li'));

    //移动到...
    var movefrom_id=0,movefrom_parent_id=0;
    function tree_moveto_recursion(selt){
        selt.each(function(){
            var $this=$(this);

            var id=$this.attr('nodeid');

            if(movefrom_id==id){
                return;
            }

            if(movefrom_parent_id!=id){
                $('<input type="button" value="移动到此处" onclick="tree_moveto('+id+','+movefrom_id+')"/>').appendTo($this.children('p'));
            }

            tree_moveto_recursion($(this).children('ul').children('li'));
        });
    }
    function tree_moveto(pid,id){
        $.post({id:id,pid:pid,op:3},function(status){
            location.reload();
        });
    }
    $('.tree_moveto').click(function(){
        if(movefrom_id){
            $(this).attr('title','移动到指定节点').text('移动到...');
            $('.tree_moveto').not(this).attr('disabled',false).removeClass('gray');
            $('ul.manage_node input,#tree_moveto').remove();
            movefrom_parent_id=0;
            movefrom_id=0;
            return;
        }
        $(this).attr('title','取消移动.').text('取消移动.');
        var $parent=$(this).parent(),$pparent=$parent.parent();
        movefrom_id=$parent.attr('nodeid');
        if(!$pparent.is('ul.manage_node')){
            movefrom_parent_id=$pparent.parent().attr('nodeid');
        }
        $('.tree_moveto').not(this).attr('disabled',true).addClass('gray');
        tree_moveto_recursion($('ul.manage_node > li'));
        $('ul.manage_node').after('<input id="tree_moveto" type="button" value="移动到此处" onclick="tree_moveto(0,'+movefrom_id+')"/>');
    });

</script>
<?php View::tplInclude('Public/footer'); ?>
