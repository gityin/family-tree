<?php
$data = array(
    'title' => empty($title)?C('SITE_TITLE'):$title,
);
View::tplInclude('Public/header', $data); ?>

<main class="bs-docs-masthead" id="content" role="main">
    <div class="tree well">
        <div align=center>
            <form action="/Admin/Member/Update" method="post">
                <input type="hidden" name="id" value="<?php echo $member['id'] ?>">
                <input type="hidden" name="pid" value="<?php echo $member['pid'] ?>">
                <div align="center" style="font-size:large;color:red">
                    <?php echo $member['name'] ?>
                </div>
                <br>
                <table align="center" cellpadding="0" cellspacing="0" style="width: 80%;height:100%">
                    <tbody>
                    <tr>
                        <td colspan="6">
                            <div align="center">
                                <strong>
                                    <span style="font-size:x-large">必 填 </span><span style="color:#FF0000">*</span>
                                </strong>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td width="4%">姓名：</td>
                        <td width="13%"><input type="text" name="name" value="<?php echo $member['name'] ?>"></td>
                        <td width="4%">性别：</td>
                        <td width="13%">
                            <select id="sex" name="sex">
                                <option value="男" <?php if('男'==$member['sex']){echo 'selected';} ?>>男</option>
                                <option value="女" <?php if('女'==$member['sex']){echo 'selected';} ?>>女</option>
                            </select></td>
                        <td width="4%">配偶：</td>
                        <td width="13%"><input type="text" name="wname" value="<?php echo $member['wname'] ?>"></td>
                    </tr>
                    <tr>
                        <td width="4%">父亲：</td>
                        <td width="13%"><input type="text" name="dad" value="<?php echo $member['dad'] ?>" readonly="readonly"/></td>
                        <td width="4%">母亲：</td>
                        <td width="13%"><input type="text" name="mother" value="<?php echo $member['mother'] ?>" readonly="readonly"/></td>
                    </tr>
                    <tr>
                        <td width="4%">长幼次序：</td>
                        <td width="13%">
                            <select name="rank">
                                <?php foreach ($rankList as $rank){ ?>
                                    <option value="<?php echo $rank ?>" <?php if($rank==$member['rank']){echo 'selected';} ?>><?php echo $rank ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td width="4%">家中排行：</td>
                        <td width="13%">
                            <select name="line">
                                <?php foreach ($familySort as $fs){ ?>
                                    <option value="<?php echo $fs ?>" <?php if($fs==$member['line']){echo 'selected';} ?>><?php echo $fs ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="4%">字辈：</td>
                        <td width="13%">
                            <select name="zibei">
                                <?php foreach ($zibeiList as $v) { ?>
                                    <option value="<?php echo $v['name'] ?>" <?php if($v['name']==$member['zibei']){echo 'selected';} ?>><?php echo $v['name'] ?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td width="4%">世代：</td>
                        <td width="13%">
                            <select name="dc">
                                <?php foreach ($zibeiList as $v){?>
                                    <option value="<?php echo $v['id'] ?>" <?php if($v['id']==$member['dc']){echo 'selected';} ?> ><?php echo $v['id'].' ('.$v['name'].')' ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="4%">兄弟：</td>
                        <td width="13%"><input type="text" name="brother" value="<?php echo $brother; ?>" readonly="readonly"/></td>
                        <td width="4%">姐妹：</td>
                        <td width="13%"><input type="text" name="sisters" value="<?php echo $sisters; ?>" readonly="readonly"/></td>

                        </td>
                    </tr>
                    <tr>
                        <td width="4%">儿子：</td>
                        <td width="13%"><input type="text" name="son" value="<?php echo $son; ?>" readonly="readonly"/>
                        <td width="4%">女儿：</td>
                        <td width="13%"><input type="text" name="daughter" value="<?php echo $daughter; ?>" readonly="readonly"/></td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td colspan="6">
                            <div align="center"><strong><span style="font-size:x-large">	<P></P>选 填 </span></strong></div>
                            <br>
                        </td>
                    </tr>
                    <tr>
                        <td width="4%">其他：</td>
                        <td colspan="5">
                            <textarea class="form-control" id="other" name="other" rows="3" style="min-width: 70%"> <?php echo $member['other'] ?></textarea>
                        </td>
                        <br>
                    </tr>
                    <tr>
                        <td width="4%">国籍：</td>
                        <td width="13%"><input type="text" name="nation" value="<?php echo $member['nation'] ?>"></td>
                        <td width="4%">籍贯：</td>
                        <td width="13%"><input type="text" name="jiguan" value="<?php echo !empty($member['jiguan']) ? $member['jiguan'] : ''; ?>"></td>

                    </tr>
                    <tr>
                        <td width="4%">曾用名：</td>
                        <td width="13%"><input type="text" name="rname" value="<?php echo $member['rname'] ?>"></td>
                        <td width="4%">生日</td>
                        <td width="13%"><input type="text" name="year" value="<?php echo $member['year'] ?>"></td>
                    </tr>
                    <tr>
                        <td width="4%">出生地：</td>
                        <td width="13%"><input type="text" name="birthadresse"
                                               value="<?php echo $member['birthadresse'] ?>"></td>
                        <td width="4%">居住地：</td>
                        <td width="13%"><input type="text" name="lifeadresse"
                                               value="<?php echo $member['lifeadresse'] ?>">
                        </td>
                        <td width="4%">通讯：</td>
                        <td width="13%"><input type="text" name="tongxun" value="<?php echo $member['tongxun'] ?>">
                        </td>
                    </tr>
                    <tr>
                        <td width="4%">婚姻：</td>
                        <td width="13%"><input type="text" name="marriage" value="<?php echo $member['marriage'] ?>">
                        </td>

                        <td width="4%">继嗣：</td>
                        <td width="13%"><input type="text" name="jisi" value="<?php echo $member['jisi'] ?>"></td>
                        <td width="4%">职业：</td>
                        <td width="13%"><input type="text" name="zhiye" value="<?php echo $member['zhiye'] ?>"></td>
                    </tr>
                    <tr>
                        <td width="4%">学历：</td>
                        <td width="13%"><input type="text" name="xueli" value="<?php echo $member['xueli'] ?>"></td>
                        <td width="4%">祭日：</td>
                        <td width="13%"><input type="text" name="dieday" value="<?php echo $member['dieday'] ?>"></td>
                        <td width="4%">墓地：</td>
                        <td width="13%"><input type="text" name="mudi" value="<?php echo $member['mudi'] ?>"></td>
                    </tr>
                    </tbody>

                </table>
                <input type="submit" value="提交">
            </form>
        </div>
        <br>
    </div>
</main>

<?php View::tplInclude('Public/footer'); ?>