<?php
$data = array(
    'title' => empty($title)?C('SITE_TITLE'):$title,
);
View::tplInclude('Public/header', $data); ?>
<main class="bs-docs-masthead" id="content" role="main">
    <div class="tree">
        <ul>
            <?php W('Tree', ['currTree'=>$tree[0],'list'=>$tree,'tpl'=>'AdminTreeWidget']);?>
        </ul>
    </div>
</main>
<?php View::tplInclude('Public/footer'); ?>
