<?php
$data = [
        'title' => empty($title)?C('SITE_TITLE'):$title,
];
?>
<link rel="stylesheet" type="text/css" href="/Statics/css/info.css"/>
<?php View::tplInclude('Public/header', $data); ?>
<main id="content" role="main">
    <div class="detail-table">
        <div align=center>
            <?php echo $member['name'] ?>
            <table align="center" cellpadding="0" cellspacing="0" style="width: 80%;height:100%">
                <?php foreach ($showFields as $k=>$field){ ?>
                <tr>
                    <td>
                        <span style="font-weight: bold;"><?php echo $field ?></span>
                    </td>
                    <td>
                        <?php echo $member[$k] ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
            <br>
            <a href="/" class="btn btn-primary">返回主页</a>
        </div>
    </div>

</main>
<?php View::tplInclude('Public/footer'); ?>
