<?php
View::tplInclude('Public/header', ['title'=>empty($title)?C('SITE_TITLE'):$title]); ?>
<main  id="content">
    <div class="tree well">
        <ul class='tree' style="border: 1px solid #999;-webkit-border-radius: 10px;">
            <div style='z-index: -1;text-align: right;color: #1b7ac5;'>截至今日，本族共繁衍 <?php echo $summary['maxdc']; ?> 代，总计 <?php echo $summary['total']; ?> 人。</div>
            <?php W('Tree', ['currTree'=>$tree[0],'list'=>$tree]);?>
        </ul>
        <div class='tree'>

            <?php foreach ($listDescribe as $k=>$item1):?>
            <h3>第<?=$k?>世 <?=$item1[0]['zibei']?>字辈</h3>
                <?php foreach ($item1 as $item2):?>
                <li>
                    <span style="<?php if($item2["sex"]=='女'){ echo "color:#ff1493";} ?>"><b><?php echo $item2["name"] ?></b></span> <?=$item2['info']?>
                </li>
                <?php endforeach;?>
            <?php endforeach;?>

        </div>
    </div>

</main>
<?php View::tplInclude('Public/footer'); ?>
