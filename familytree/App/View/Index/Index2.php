<?php
View::tplInclude('Public/header', ['title'=>empty($title)?C('SITE_TITLE'):$title]); ?>
<main  id="content">
    <div class="tree well">
        <?php foreach ($treeList as $item):?>
        <span style="font-weight: bold;font-size:20px;margin: 23px"><?=$item['0']['name']?>的后代</span>
        <ul class='tree' style="border: 1px solid #999;-webkit-border-radius: 10px;">
            <?php W('Tree2', ['currTree'=>$item]);?>
        </ul>
        <?php endforeach;?>
    </div>

</main>
<?php View::tplInclude('Public/footer'); ?>
