<?php
class TreeWidget extends Widget {
    public function invoke($data){
        $this->assign('list', $data['list']);
        $this->assign('currTree', $data['currTree']);
        $tpl = 'TreeWidget';
        if(isset($data['tpl']) && $data['tpl']){
            $tpl = $data['tpl'];
        }
        $this->display($tpl);
    }
}
