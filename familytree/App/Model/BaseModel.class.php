<?php
class BaseModel extends Model {
    public $_table = '';

    public function __construct()
    {
        if(empty($this->_table)){
            $cls = get_called_class();
            $this->_table = strstr($cls,'Model',true);
            $this->_table = strtolower($this->_table);
        }

        parent::__construct();
    }

}
