<?php
class TreeModel extends BaseModel {
    public $_table = 'tree';

    public function getSummary(){
        $data = $this->_db->query('SELECT COUNT(*) AS total, (MAX(dc)-MIN(dc)+1) AS maxdc FROM tree WHERE name NOT LIKE \'%出%\'');
        return $data[0];
    }
}
