<?php
class AdminHomeController extends BaseController {
    public function IndexAction(){
        $treeModel = new TreeModel();
        $tree = $treeModel->orderBy('dorder','asc')->select('id, name, pid, sex, dc,zibei');
        $list = [];
        foreach ($tree as $item){
            $pt = $item['pid'];
            $list[$pt][] = $item;
        }
        $summary = $treeModel->getSummary();

        $this->assign('tree', $list);
        $this->assign('summary', $summary);
        $this->assign('title', '');
        $this->display();
    }

}
