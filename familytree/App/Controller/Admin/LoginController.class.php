<?php
class AdminLoginController extends BaseController {
    public function IndexAction(){
        $this->assign('title', 'Login');
        $this->display();
    }

    public function LoginAction(){
        $error = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $_POST['username'];
            $password = $_POST['password'];
            if ($this->_checkUser($username,$password)) {
                $_SESSION['username'] = $username;
                // 重定向回原始页面
                $url = isset($_GET['url']) ? $_GET['url'] : '/Admin/Home/Index';
                $this->redirect($url);
                exit();
            } else {
                // 如果用户名或密码不正确，显示错误消息
                $error = 'Invalid username or password';
            }
        }
        $this->assign('title', 'Login');
        $this->assign('error', $error);
        $this->display('index');
    }

    public function LogoutAction(){
        session_unset();
    }

    private function _checkUser($username,$password){
        $userModel = new UserModel();
        $member = $userModel->where(['username'=>$username])->get();
        if($member){
            return password_verify($password,$member['password']);
        }
        return false;
    }
}
