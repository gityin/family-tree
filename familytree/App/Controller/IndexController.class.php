<?php
class IndexController extends BaseController {
    public function IndexAction(){
        $treeModel = new TreeModel();
        $tree = $treeModel->orderBy('dorder','asc')->select();
        $list = [];
        foreach ($tree as $item){
            $list[$item['pid']][] =[
                'id'=>$item['id'],
                'name'=>$item['name'],
                'pid'=>$item['pid'],
                'sex'=>$item['sex'],
                'dc'=>$item['dc'],
                'zibei'=>$item['zibei']
            ];

            //按辈分展示展示宗亲人员全部信息
            $listDescribe[$item['dc']][] = [
                'name'=>$item['name'],
                'sex'=>$item['sex'],
                'zibei'=>$item['zibei'],
                'info' => $item['info'] //生平简介
            ];
        }

        $summary = $treeModel->getSummary();
        $this->assign('tree', $list);
        $this->assign('listDescribe', $listDescribe);
        $this->assign('summary', $summary);
        $this->display();
    }
    //指定多个父级 返回多个树形结构  完整的树形结果
    public function Index2Action(){
        $treeModel = new TreeModel();
        //指定父级
        $idList = ['2','3'];
        foreach ($idList as $id){
            $tree = $treeModel->where(['id'=>$id])->field('id, name, pid, sex, dc,zibei')->get();
            $list = $this->_getChildren($tree['id'],$tree);
            $treeList[] = [$list];
        }
        $this->assign('treeList', $treeList);
        $this->display();
    }

    private function _getChildren($id,$currMemberInfo){
        $treeModel = new TreeModel();
        $childrens = $treeModel->where(['pid'=>$id])->orderBy('dorder','asc')->select('id, name, pid, sex, dc,zibei');
        if($childrens){
            foreach ($childrens as $children){
                $currMemberInfo['children'][] = $this->_getChildren($children['id'],$children);
            }
        }
        return $currMemberInfo;
    }

    public function DetailAction(){
        $treeModel = new TreeModel();
        $member = $treeModel->get($_GET['id']);
        $this->assign('member', $member);

        $memberObj = new Member();
        $showFields= $memberObj->getShowFields();
        $this->assign('showFields', $showFields);

        $this->display();
    }

}
