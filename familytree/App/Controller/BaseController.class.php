<?php
class BaseController extends Controller{

    protected function _init(){
        header("Content-Type:text/html; charset=utf-8");
        $cls = get_called_class();
        if('AdminLoginController'!==$cls && substr($cls,0,5)=='Admin'){
            if(!isLogin()){
                $this->redirect('/admin/login?url='. urlencode($_SERVER['REQUEST_URI']));
            }
        }
    }
} 
