<?php
$config = [
    'USE_SESSION' => true,//是否开启session，默认false
    'PATH_MOD' => env('PATH_MOD', 'NORMAL'),//NORMAL
    'SITE_TITLE' => env('SITE_TITLE', ''),
    'WRITE_LOG' => env('WRITE_LOG', true),

    #数据库配置
    'DB_HOST'    => env('DB_HOST'),
    'DB_PORT'    => env('DB_PORT'),
    'DB_USER'    => env('DB_USER'),
    'DB_PWD'     => env('DB_PWD'),
    'DB_NAME'    => env('DB_NAME'),
    'DB_CHARSET' => env('DB_CHARSET'),

    #加密配置
    'AES_SECRET_KEY' => env('AES_SECRET_KEY', '123456'),
];

//log等级
define('LOG_LEVEL', 5);

/*define('LOG_ERR', 3);//致命错误
define('LOG_WARNING', 4);//警告
define('LOG_NOTICE', 5);//通知
define('LOG_INFO', 6);
define('LOG_DEBUG', 7);*/

