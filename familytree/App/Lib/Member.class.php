<?php

class Member
{
    public function getShowFields()
    {

        $showFields = [
            'name' => '姓名',
            'rname' => '曾用名',
            'sex' => '性别',
            'wname' => '配偶',
            'rank' => '家中长幼次序',
            'line' => '家中排行',
            'dc' => '世代',
            'zibei' => '字辈',
            'dad' => '父亲',
            'mother' => '母亲',
            'brother' => '兄弟',
            'sisters' => '姐妹',
            'son' => '儿子',
            'daughter' => '女儿',
            'nation' => '国籍',
            'jiguan' => '籍贯',
            //'zi'=>'字',
            //'hao'=>'号',
            'year' => '生日',
            'birthadresse' => '出生地',
            'lifeadresse' => '居住地',
            'tongxun' => '通讯',
            'marriage' => '婚姻',
            'sisters' => '姐妹',
            'jisi' => '继嗣',
            'zhiye' => '职业',
            'xueli' => '学历',
            //'info'=>'生平简介',
            'other' => '其它',
            'dieday' => '祭日',
            'mudi' => '墓地',
        ];
        return $showFields;
    }

    //长幼次序
    public function getRankList()
    {
        $rank1 = array("长子", "次子", "三子", "四子", "五子", "六子", "七子", "八子");
        $rank2 = array("长女", "次女", "三女", "四女", "五女", "六女", "七女", "八女");
        return array_merge($rank1, $rank2);
    }

    //家中排行
    public function getFamilySort()
    {
        return ['老大', '老二', '老三', '老四', '老五', '老六', '老七', '老八'];
    }

    //生平简介
    public function getBiography($id,$isSave=false)
    {
        $treeModel = new TreeModel();
        if ($id) {
            $item = $treeModel->where(['id' => $id])->get();
        }
        $year = $item['year'] ? '生于' . $item['year'] : '';
        $dieday = $item['dieday'] ? '殁于' . $item['dieday'] : '';
        if ($year && $dieday) {
            $year .= "，";
            $dieday .= "。";
        } elseif ($year || $dieday) {
            $dieday .= "。";
        }
        $dad = $item['dad'] ? $item['dad'] . $item['rank'] : '';//XXX长子 行大
        $mother = $item['mother'] ? '母亲：' . $item['mother'] : '';
        if($dad && $item['line']){
            $dad = $dad.' 行'.str_replace('老','',$item['line']);
        }
        if ($dad && $mother) {
            $dad .= "，";
            $mother .= "。";
        } elseif ($dad || $mother) {
            $mother .= "。";
        }
        //女子不在记录子女信息
        $son = $daughter = $spouse = '';
        if ($item['sex'] == '女') {
            $spouse = $item['wname'] ? '嫁' . $item['wname'] : '';
            if ($spouse) {
                $spouse .= "。";
            }
        } else {
            $spouse = $item['wname'] ? '娶' . $item['wname'] : '';
            if ($spouse) {
                $spouse .= "，";
            }
            $son = '生子：' . $item['son'] . "，";
            $daughter = '生女：' . $item['daughter'] . "。";
        }

        $brother = '兄弟：' . $item['brother'] . "，";
        $sisters = '姐妹：' . $item['sisters'] . "。";
        $other = trim($item['other']) ? $item['other'] . "。" : "";

        $infoChain = $year . $dieday . $dad . $mother . $spouse . $son . $daughter . $brother . $sisters . $other;
        if($isSave){
            $treeModel->where(['id' => $id])->update(['info'=>$infoChain]);
        }
        return $infoChain;
    }
}
