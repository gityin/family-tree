<?php

class Aes
{
    static private $iv='0F72EEE99DBBA121';

    // 加密
    static public function encrypt($data,$key=''){
        // 密钥
        if(empty($key)){
            $key = C('AES_SECRET_KEY');
        }
        $encrypted = openssl_encrypt($data, "AES-128-CBC", $key, OPENSSL_RAW_DATA, self::$iv);
        // 密文
        $encryptedText = base64_encode($encrypted);
        return $encryptedText;
    }

    // 解密
    static public function decrypt($data,$key=''){
        // 密钥
        if(empty($key)){
            $key = C('AES_SECRET_KEY');
        }
        $decryptedText = openssl_decrypt(base64_decode($data), "AES-128-CBC", $key, OPENSSL_RAW_DATA, self::$iv);
        return $decryptedText;
    }



}
