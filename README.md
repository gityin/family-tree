# 家谱-familytree

### 简介

此项目是由[SinglePHP](https://github.com/leo108/SinglePHP) 框架+[familytree](https://github.com/jiehu0992/5itv) 项目改造而成

familytree项目没有框架，所以我用了个SinglePHP框架套了一下

目前用的样式是：word现代版家谱
![前端展示](familytree/Statics/images/2023-08-17T02-57-45.320Z.png)

### 安装说明

项目入口文件：familytree/index.php

建个数据库，执行一下db.sql文件的SQL

修改.env.example文件名为.env  并将数据库信息配置进去